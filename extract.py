# encoding: UTF-8

from __future__ import print_function

import c3d_pb2 as c3d
import numpy as np
from timer import Timer


def extract_value(blob):
    return np.array(blob.data).reshape(list(blob.shape.dim))


def extract_conv3d(layer):
    param = layer.convolution3d_param
    assert param is not None and len(layer.blobs) is 1
    weights, = map(extract_value, layer.blobs)
    # Caffe format [out_channels, in_channels, filter_depth, filter_height, filter_width]
    # TF    format [filter_depth, filter_height, filter_width, in_channels, out_channels]
    weights = weights.transpose([2, 3, 4, 1, 0])
    print('\tkernel:', weights.shape)
    return {'kernel:0': weights}


def extract_bn(bn, scale):
    mean, variance, moving_average = map(extract_value, bn.blobs)
    moving_average = moving_average[0]
    scaling_factor = 1.0 / moving_average if moving_average != 0 else 0
    mean *= scaling_factor
    variance *= scaling_factor
    beta, gamma = map(extract_value, scale.blobs)
    print('\tbeta:', beta.shape)
    print('\tgamma:', gamma.shape)
    print('\tmean:', mean.shape)
    print('\tvar:', variance.shape)
    return {'beta:0': beta, 'gamma:0': gamma}


def load_caffemodel(filename):
    # This operation costs ~2 minutes
    with Timer('Parse caffemodel'):
        net = c3d.NetParameter()
        with open(filename, 'rb') as caffemodel:
            model = caffemodel.read()
            net.MergeFromString(model)
    return net


def extract(net):
    params = {}
    bn = None
    with Timer('Extract parameters'):
        for i, layer in enumerate(net.layer):
            if layer.type == 'BatchNorm':
                print('Found {}Layer {!r}, looking for ScaleLayer'.format(layer.type, layer.name))
                bn = i, layer
            elif layer.type == 'Scale' and i is bn[0] + 1:
                print('Found {}Layer {!r}, extracting mean / var / gamma / beta'.format(layer.type, layer.name))
                values = extract_bn(bn[1], layer)
            elif layer.type == 'Convolution3D':
                print('Found {}Layer {!r}, extracting kernel'.format(layer.type, layer.name))
                values = extract_conv3d(layer)
            else:
                continue
            for variable, value in values.items():
                params['{}/{}'.format(layer.name, variable)] = value
    return params


def main():
    filename = 'finetuning.caffemodel'
    net = load_caffemodel(filename)
    params = extract(net)
    with Timer('Compress parameters'):
        np.savez('finetuning.npz', **params)
        pass


if __name__ == '__main__':
    main()
