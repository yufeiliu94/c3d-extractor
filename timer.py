# encoding: UTF-8

import time


class Timer(object):
    def __init__(self, hint=None):
        self.hint = hint
        self.begin = None

    def __enter__(self):
        self.begin = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        duration = time.time() - self.begin
        cost = '{:.3f} seconds'.format(duration)
        if self.hint is not None:
            print('{}: {}'.format(self.hint, cost))
        else:
            print(cost)

