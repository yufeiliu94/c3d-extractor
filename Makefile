PROTOBUF=https://raw.githubusercontent.com/facebook/C3D/master/C3D-v1.1/src/caffe/proto/caffe.proto
CAFFEMODEL=https://www.dropbox.com/s/qqfrg6h44d4jb46/c3d_resnet18_sports1m_r2_iter_2800000.caffemodel

finetuning.caffemodel:
	wget $(CAFFEMODEL) -O $@

c3d: c3d.proto
	protoc $^ --python_out=.

c3d.proto:
	wget $(PROTOBUF) -O $@

.PHONY: clean

clean:
	rm -f c3d.proto c3d_pb2.py finetuning.caffemodel
